############################################################
# Dockerfile to build Jetty container images
############################################################
# Set the base image to Ubuntu
FROM registry.gitlab.com/secupi/jetty-image:latest

# File Author / Maintainer
MAINTAINER rotem@secupi.com

################## BEGIN INSTALLATION ######################
RUN apt-get update && apt-get install -y wget && apt-get install nano && apt-get install -y git
##################### INSTALLATION END #####################

